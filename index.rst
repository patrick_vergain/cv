
.. figure:: images/patrick_vergain_2022_08.png
   :align: center
   :width: 200


.. _patrick_vergain:

=====================
**Patrick Vergain**
=====================

TL; DR
=======

Développeur C/Python/django/PostgreSQL/docker/docker-compose, + Documentation (sphinx), etc.


.. toctree::
   :maxdepth: 3

   entreprises/entreprises
   sous_traitance/sous_traitance
   formations/formations
